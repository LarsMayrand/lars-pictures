const NUM_PHOTOS = 46;

var slideIndex = 1,
	windowWidth,
	isMobile;

var carousel = document.getElementById("carousel"),
	page = document.getElementById("page"),
	slides = document.getElementById("slides");

loadGallery();
loadCarousel();
showSlides(slideIndex);
// size();

// addEventListener("resize", size, false);

// function size() {
// 	windowWidth = window.innerWidth;
// }

if (isMobile && windowWidth > 800) {
	isMobile = false;
}

function toggleMobileMenu(menu) {
	menu.classList.toggle("open");
}

carousel.addEventListener("click", (e) => {
	if (e.target === e.currentTarget) {
		closeCarousel();
	}
});

/** Populates gallery. */
function loadGallery() {
	var viewportWidth = Math.max(
		document.documentElement.clientWidth,
		window.innerWidth || 0
	);
	// let columns = viewportWidth < 800 ? 2 : 3;
	let columns = 3;
	for (let i = 0; i < columns; i++) {
		let column = document.getElementById("column_" + i);
		for (let j = i; j < NUM_PHOTOS; j += columns) {
			column.innerHTML += `<img src="photos/previews/400-${
				j + 1
			}.jpg" class="preview-image" onclick="openCarousel();currentSlide(${
				j + 1
			})">`;
		}
	}
}

/** Populates lightbox. */
function loadCarousel() {
	for (let i = 0; i < NUM_PHOTOS; i++) {
		slides.innerHTML += `<div class="slides"><img src="photos/1080/1080-${
			i + 1
		}.jpg"/></div>`;
	}
}

function openCarousel() {
	carousel.style.display = "block";
	document.getElementsByTagName("body")[0].classList.add("no-scroll");
}

function closeCarousel() {
	carousel.style.display = "none";
	document.getElementsByTagName("body")[0].classList.remove("no-scroll");
}

function updateSlide(n) {
	showSlides((slideIndex += n));
}

function currentSlide(n) {
	showSlides((slideIndex = n));
}

function showSlides(n) {
	let slidesArray = slides.children;
	if (n > slidesArray.length) {
		slideIndex = 1;
	}
	if (n < 1) {
		slideIndex = slidesArray.length;
	}
	[...slidesArray].forEach((slide) => (slide.style.display = "none"));
	slidesArray[slideIndex - 1].style.display = "block";
}

// handling Internet Explorer issue with window.event
// @see http://stackoverflow.com/a/3985882/517705
function checkKeycode(event) {
	let keyDownEvent = event || window.event,
		keycode = keyDownEvent.which
			? keyDownEvent.which
			: keyDownEvent.keyCode;
	if (keycode == 37) {
		// left arrow
		updateSlide(-1);
	} else if (keycode == 39) {
		// right arrow
		updateSlide(1);
	} else if (keycode == 27) {
		closeCarousel();
	}
}

document.onkeydown = checkKeycode;
